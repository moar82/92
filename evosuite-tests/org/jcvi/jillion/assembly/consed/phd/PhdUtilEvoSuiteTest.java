/*
 * This file was automatically generated by EvoSuite
 */

package org.jcvi.jillion.assembly.consed.phd;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import java.text.ParseException;
import java.util.Date;
import java.util.Map;
import org.jcvi.jillion.assembly.consed.phd.PhdUtil;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class PhdUtilEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      Date date0 = new Date();
      Map<String, String> map0 = PhdUtil.createPhdTimeStampAndChromatFileCommentsFor(date0, "");
      assertEquals(1372731255899L, date0.getTime());
      assertEquals(2, map0.size());
  }

  @Test
  public void test1()  throws Throwable  {
      try {
        PhdUtil.parseReadTagDate("CHROMAT_FILE");
        fail("Expecting exception: ParseException");
      } catch(ParseException e) {
        /*
         * Unparseable date: \"CHROMAT_FILE\"
         */
      }
  }

  @Test
  public void test2()  throws Throwable  {
      Date date0 = new Date();
      String string0 = PhdUtil.formatReadTagDate(date0);
      assertNotNull(string0);
      assertEquals(1372731255970L, date0.getTime());
  }

  @Test
  public void test3()  throws Throwable  {
      Date date0 = new Date();
      PhdUtil.createPhdTimeStampCommentFor(date0);
      assertEquals(1372731255986L, date0.getTime());
  }
}
