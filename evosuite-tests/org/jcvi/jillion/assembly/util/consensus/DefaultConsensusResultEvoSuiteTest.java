/*
 * This file was automatically generated by EvoSuite
 */

package org.jcvi.jillion.assembly.util.consensus;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import org.jcvi.jillion.assembly.util.consensus.DefaultConsensusResult;
import org.jcvi.jillion.core.residue.nt.Nucleotide;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class DefaultConsensusResultEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      DefaultConsensusResult defaultConsensusResult0 = new DefaultConsensusResult((Nucleotide) null, (-1763));
      defaultConsensusResult0.getConsensus();
      assertEquals("null[-1763]", defaultConsensusResult0.toString());
      assertEquals(-1763, defaultConsensusResult0.getConsensusQuality());
  }

  @Test
  public void test1()  throws Throwable  {
      DefaultConsensusResult defaultConsensusResult0 = new DefaultConsensusResult((Nucleotide) null, (-1763));
      int int0 = defaultConsensusResult0.getConsensusQuality();
      assertEquals((-1763), int0);
  }

  @Test
  public void test2()  throws Throwable  {
      DefaultConsensusResult defaultConsensusResult0 = new DefaultConsensusResult((Nucleotide) null, (-1763));
      String string0 = defaultConsensusResult0.toString();
      assertNotNull(string0);
      assertEquals("null[-1763]", string0);
  }

  @Test
  public void test3()  throws Throwable  {
      Nucleotide nucleotide0 = Nucleotide.Pyrimidine;
      DefaultConsensusResult defaultConsensusResult0 = new DefaultConsensusResult(nucleotide0, 0);
      int int0 = defaultConsensusResult0.hashCode();
      assertEquals(1886707313, int0);
  }

  @Test
  public void test4()  throws Throwable  {
      DefaultConsensusResult defaultConsensusResult0 = new DefaultConsensusResult((Nucleotide) null, (-1763));
      int int0 = defaultConsensusResult0.hashCode();
      assertEquals((-802), int0);
  }

  @Test
  public void test5()  throws Throwable  {
      DefaultConsensusResult defaultConsensusResult0 = new DefaultConsensusResult((Nucleotide) null, (-1763));
      boolean boolean0 = defaultConsensusResult0.equals((Object) null);
      assertEquals(-1763, defaultConsensusResult0.getConsensusQuality());
      assertEquals(false, boolean0);
  }

  @Test
  public void test6()  throws Throwable  {
      DefaultConsensusResult defaultConsensusResult0 = new DefaultConsensusResult((Nucleotide) null, (-1763));
      Nucleotide nucleotide0 = Nucleotide.NotAdenine;
      DefaultConsensusResult defaultConsensusResult1 = new DefaultConsensusResult(nucleotide0, 72);
      boolean boolean0 = defaultConsensusResult0.equals((Object) defaultConsensusResult1);
      assertEquals(false, boolean0);
      assertEquals(-1763, defaultConsensusResult0.getConsensusQuality());
  }

  @Test
  public void test7()  throws Throwable  {
      Nucleotide nucleotide0 = Nucleotide.Unknown;
      DefaultConsensusResult defaultConsensusResult0 = new DefaultConsensusResult(nucleotide0, (-1));
      boolean boolean0 = defaultConsensusResult0.equals((Object) "N[0]");
      assertEquals("N[-1]", defaultConsensusResult0.toString());
      assertEquals(false, boolean0);
  }

  @Test
  public void test8()  throws Throwable  {
      Nucleotide nucleotide0 = Nucleotide.Keto;
      DefaultConsensusResult defaultConsensusResult0 = new DefaultConsensusResult(nucleotide0, 67);
      DefaultConsensusResult defaultConsensusResult1 = new DefaultConsensusResult(nucleotide0, 67);
      boolean boolean0 = defaultConsensusResult0.equals((Object) defaultConsensusResult1);
      assertEquals(true, boolean0);
      assertEquals(67, defaultConsensusResult0.getConsensusQuality());
  }

  @Test
  public void test9()  throws Throwable  {
      DefaultConsensusResult defaultConsensusResult0 = new DefaultConsensusResult((Nucleotide) null, (-1763));
      DefaultConsensusResult defaultConsensusResult1 = new DefaultConsensusResult((Nucleotide) null, 72);
      boolean boolean0 = defaultConsensusResult0.equals((Object) defaultConsensusResult1);
      assertEquals("null[72]", defaultConsensusResult1.toString());
      assertFalse(defaultConsensusResult1.equals(defaultConsensusResult0));
      assertEquals(false, boolean0);
  }

  @Test
  public void test10()  throws Throwable  {
      Nucleotide nucleotide0 = Nucleotide.Unknown;
      Nucleotide nucleotide1 = Nucleotide.Gap;
      DefaultConsensusResult defaultConsensusResult0 = new DefaultConsensusResult(nucleotide0, (-1));
      DefaultConsensusResult defaultConsensusResult1 = new DefaultConsensusResult(nucleotide1, 1252);
      boolean boolean0 = defaultConsensusResult0.equals((Object) defaultConsensusResult1);
      assertEquals(false, boolean0);
      assertEquals("-[1252]", defaultConsensusResult1.toString());
  }
}
