/*
 * This file was automatically generated by EvoSuite
 */

package org.jcvi.jillion.core.residue.aa;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import org.jcvi.jillion.core.residue.aa.CompactAminoAcidSequence;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class CompactAminoAcidSequenceEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      CompactAminoAcidSequence compactAminoAcidSequence0 = new CompactAminoAcidSequence("");
  }
}
