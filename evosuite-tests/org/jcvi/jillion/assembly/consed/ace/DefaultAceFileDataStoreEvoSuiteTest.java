/*
 * This file was automatically generated by EvoSuite
 */

package org.jcvi.jillion.assembly.consed.ace;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import org.jcvi.jillion.assembly.consed.ace.DefaultAceFileDataStore;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class DefaultAceFileDataStoreEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      PipedOutputStream pipedOutputStream0 = new PipedOutputStream();
      PipedInputStream pipedInputStream0 = new PipedInputStream(pipedOutputStream0, 1396);
      pipedOutputStream0.close();
      // Undeclared exception!
      try {
        DefaultAceFileDataStore.create((InputStream) pipedInputStream0);
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test1()  throws Throwable  {
      // Undeclared exception!
      try {
        DefaultAceFileDataStore.create((File) null);
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }
}
