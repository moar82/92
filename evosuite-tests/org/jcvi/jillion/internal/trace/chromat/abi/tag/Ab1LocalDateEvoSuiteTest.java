/*
 * This file was automatically generated by EvoSuite
 */

package org.jcvi.jillion.internal.trace.chromat.abi.tag;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import java.util.Date;
import org.jcvi.jillion.internal.trace.chromat.abi.tag.Ab1LocalDate;
import org.jcvi.jillion.internal.trace.chromat.abi.tag.Ab1LocalTime;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class Ab1LocalDateEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      Ab1LocalDate ab1LocalDate0 = new Ab1LocalDate((-1), 32, 32);
      int int0 = ab1LocalDate0.hashCode();
      assertEquals(61534, int0);
  }

  @Test
  public void test1()  throws Throwable  {
      Ab1LocalDate ab1LocalDate0 = new Ab1LocalDate(0, 1, (-1));
      int int0 = ab1LocalDate0.getDay();
      assertEquals(1, ab1LocalDate0.getMonth());
      assertEquals((-1), int0);
      assertEquals(0, ab1LocalDate0.getYear());
  }

  @Test
  public void test2()  throws Throwable  {
      Ab1LocalDate ab1LocalDate0 = new Ab1LocalDate((-1996), (-1996), (-1996));
      Ab1LocalTime ab1LocalTime0 = new Ab1LocalTime((-1996), 0, 31);
      Date date0 = ab1LocalDate0.toDate(ab1LocalTime0);
      assertNotNull(date0);
      assertEquals((-130585147169000L), date0.getTime());
  }

  @Test
  public void test3()  throws Throwable  {
      Ab1LocalDate ab1LocalDate0 = new Ab1LocalDate((-1), 32, 32);
      int int0 = ab1LocalDate0.getYear();
      assertEquals(32, ab1LocalDate0.getMonth());
      assertEquals(32, ab1LocalDate0.getDay());
      assertEquals((-1), int0);
  }

  @Test
  public void test4()  throws Throwable  {
      Ab1LocalDate ab1LocalDate0 = new Ab1LocalDate((-1), 0, (-1));
      int int0 = ab1LocalDate0.getMonth();
      assertEquals(0, int0);
      assertEquals(-1, ab1LocalDate0.getYear());
      assertEquals(-1, ab1LocalDate0.getDay());
  }

  @Test
  public void test5()  throws Throwable  {
      Ab1LocalDate ab1LocalDate0 = new Ab1LocalDate((-1996), (-1996), (-1996));
      Ab1LocalDate ab1LocalDate1 = new Ab1LocalDate((-1996), 31, (-1996));
      boolean boolean0 = ab1LocalDate0.equals((Object) ab1LocalDate1);
      assertEquals(false, boolean0);
      assertFalse(ab1LocalDate1.equals(ab1LocalDate0));
      assertEquals(-1996, ab1LocalDate1.getYear());
      assertEquals(-1996, ab1LocalDate1.getDay());
      assertEquals(-1996, ab1LocalDate0.getMonth());
  }

  @Test
  public void test6()  throws Throwable  {
      Ab1LocalDate ab1LocalDate0 = new Ab1LocalDate(31, 0, 31);
      boolean boolean0 = ab1LocalDate0.equals((Object) null);
      assertEquals(false, boolean0);
      assertEquals(31, ab1LocalDate0.getDay());
      assertEquals(31, ab1LocalDate0.getYear());
      assertEquals(0, ab1LocalDate0.getMonth());
  }

  @Test
  public void test7()  throws Throwable  {
      Ab1LocalDate ab1LocalDate0 = new Ab1LocalDate(0, (-1901), 0);
      boolean boolean0 = ab1LocalDate0.equals((Object) "");
      assertEquals(false, boolean0);
      assertEquals(0, ab1LocalDate0.getDay());
      assertEquals(0, ab1LocalDate0.getYear());
      assertEquals(-1901, ab1LocalDate0.getMonth());
  }

  @Test
  public void test8()  throws Throwable  {
      Ab1LocalDate ab1LocalDate0 = new Ab1LocalDate(0, (-1901), 0);
      Ab1LocalDate ab1LocalDate1 = new Ab1LocalDate(0, 706, (-735));
      boolean boolean0 = ab1LocalDate0.equals((Object) ab1LocalDate1);
      assertEquals(-735, ab1LocalDate1.getDay());
      assertEquals(0, ab1LocalDate1.getYear());
      assertEquals(-1901, ab1LocalDate0.getMonth());
      assertEquals(false, boolean0);
  }

  @Test
  public void test9()  throws Throwable  {
      Ab1LocalDate ab1LocalDate0 = new Ab1LocalDate((-1786), 1, 1);
      Ab1LocalDate ab1LocalDate1 = new Ab1LocalDate((-1786), 1, 1);
      boolean boolean0 = ab1LocalDate0.equals((Object) ab1LocalDate1);
      assertEquals(1, ab1LocalDate0.getMonth());
      assertEquals(1, ab1LocalDate0.getDay());
      assertEquals(-1786, ab1LocalDate0.getYear());
      assertEquals(true, boolean0);
  }

  @Test
  public void test10()  throws Throwable  {
      Ab1LocalDate ab1LocalDate0 = new Ab1LocalDate(3, 778, 0);
      Ab1LocalDate ab1LocalDate1 = new Ab1LocalDate(778, 778, 0);
      boolean boolean0 = ab1LocalDate0.equals((Object) ab1LocalDate1);
      assertEquals(3, ab1LocalDate0.getYear());
      assertFalse(ab1LocalDate1.equals(ab1LocalDate0));
      assertEquals(false, boolean0);
      assertEquals(778, ab1LocalDate0.getMonth());
      assertEquals(0, ab1LocalDate0.getDay());
  }
}
