/*
 * This file was automatically generated by EvoSuite
 */

package org.jcvi.jillion.assembly.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.PriorityQueue;
import java.util.Stack;
import java.util.TreeSet;
import org.jcvi.jillion.assembly.util.CoverageRegion;
import org.jcvi.jillion.assembly.util.DefaultCoverageRegion;
import org.jcvi.jillion.core.DirectedRange;
import org.jcvi.jillion.core.Range;
import org.jcvi.jillion.core.Rangeable;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class DefaultCoverageRegionEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      PriorityQueue<CoverageRegion<Rangeable>> priorityQueue0 = new PriorityQueue<CoverageRegion<Rangeable>>();
      Iterator<CoverageRegion<Rangeable>> iterator0 = priorityQueue0.iterator();
      Integer integer0 = new Integer(248);
      DefaultCoverageRegion.Builder<CoverageRegion<Rangeable>> defaultCoverageRegion_Builder0 = new DefaultCoverageRegion.Builder<CoverageRegion<Rangeable>>((-1281L), iterator0, integer0);
      assertNotNull(defaultCoverageRegion_Builder0);
      
      Collection<CoverageRegion<Rangeable>> collection0 = defaultCoverageRegion_Builder0.getElements();
      assertEquals((-1281L), defaultCoverageRegion_Builder0.start());
      assertNotNull(collection0);
  }

  @Test
  public void test1()  throws Throwable  {
      ArrayList<Rangeable> arrayList0 = new ArrayList<Rangeable>();
      Iterator<Rangeable> iterator0 = arrayList0.iterator();
      DefaultCoverageRegion.Builder<Rangeable> defaultCoverageRegion_Builder0 = new DefaultCoverageRegion.Builder<Rangeable>((-1686L), iterator0);
      assertNotNull(defaultCoverageRegion_Builder0);
      
      long long0 = defaultCoverageRegion_Builder0.start();
      assertEquals((-1686L), long0);
  }

  @Test
  public void test2()  throws Throwable  {
      PriorityQueue<Rangeable> priorityQueue0 = new PriorityQueue<Rangeable>();
      Integer integer0 = new Integer(621);
      DefaultCoverageRegion.Builder<Rangeable> defaultCoverageRegion_Builder0 = new DefaultCoverageRegion.Builder<Rangeable>((-1L), (Iterable<Rangeable>) priorityQueue0, integer0);
      assertNotNull(defaultCoverageRegion_Builder0);
      
      DefaultCoverageRegion.Builder<Rangeable> defaultCoverageRegion_Builder1 = (DefaultCoverageRegion.Builder<Rangeable>)defaultCoverageRegion_Builder0.removeAll((Collection) priorityQueue0);
      assertEquals((-1L), defaultCoverageRegion_Builder1.start());
      assertNotNull(defaultCoverageRegion_Builder1);
  }

  @Test
  public void test3()  throws Throwable  {
      PriorityQueue<Rangeable> priorityQueue0 = new PriorityQueue<Rangeable>();
      Integer integer0 = new Integer(621);
      DefaultCoverageRegion.Builder<Rangeable> defaultCoverageRegion_Builder0 = new DefaultCoverageRegion.Builder<Rangeable>((-1L), (Iterable<Rangeable>) priorityQueue0, integer0);
      assertNotNull(defaultCoverageRegion_Builder0);
      
      DefaultCoverageRegion.Builder<Rangeable> defaultCoverageRegion_Builder1 = (DefaultCoverageRegion.Builder<Rangeable>)defaultCoverageRegion_Builder0.end((-1L));
      DefaultCoverageRegion<Rangeable> defaultCoverageRegion0 = (DefaultCoverageRegion<Rangeable>)defaultCoverageRegion_Builder1.build();
      assertEquals(true, defaultCoverageRegion_Builder0.isEndIsSet());
      
      Stack<CoverageRegion<Rangeable>> stack0 = new Stack<CoverageRegion<Rangeable>>();
      ListIterator<CoverageRegion<Rangeable>> listIterator0 = stack0.listIterator();
      DefaultCoverageRegion.Builder<CoverageRegion<Rangeable>> defaultCoverageRegion_Builder2 = new DefaultCoverageRegion.Builder<CoverageRegion<Rangeable>>((-1L), (Iterator<CoverageRegion<Rangeable>>) listIterator0);
      defaultCoverageRegion_Builder2.remove((Rangeable) defaultCoverageRegion0);
      assertEquals("coverage region : [ -1 .. -1 ]/0B coverage = 0", defaultCoverageRegion0.toString());
  }

  @Test
  public void test4()  throws Throwable  {
      HashSet<CoverageRegion<Rangeable>> hashSet0 = new HashSet<CoverageRegion<Rangeable>>();
      Iterator<CoverageRegion<Rangeable>> iterator0 = hashSet0.iterator();
      DefaultCoverageRegion.Builder<CoverageRegion<Rangeable>> defaultCoverageRegion_Builder0 = new DefaultCoverageRegion.Builder<CoverageRegion<Rangeable>>(1239L, iterator0);
      assertNotNull(defaultCoverageRegion_Builder0);
      
      // Undeclared exception!
      try {
        defaultCoverageRegion_Builder0.end();
        fail("Expecting exception: IllegalArgumentException");
      } catch(IllegalArgumentException e) {
        /*
         * end not yet set
         */
      }
  }

  @Test
  public void test5()  throws Throwable  {
      PriorityQueue<Rangeable> priorityQueue0 = new PriorityQueue<Rangeable>();
      Integer integer0 = new Integer(621);
      DefaultCoverageRegion.Builder<Rangeable> defaultCoverageRegion_Builder0 = new DefaultCoverageRegion.Builder<Rangeable>((-1L), (Iterable<Rangeable>) priorityQueue0, integer0);
      assertNotNull(defaultCoverageRegion_Builder0);
      
      DefaultCoverageRegion.Builder<Rangeable> defaultCoverageRegion_Builder1 = (DefaultCoverageRegion.Builder<Rangeable>)defaultCoverageRegion_Builder0.end((-1L));
      DefaultCoverageRegion<Rangeable> defaultCoverageRegion0 = (DefaultCoverageRegion<Rangeable>)defaultCoverageRegion_Builder1.build();
      assertEquals(true, defaultCoverageRegion_Builder0.isEndIsSet());
      
      String string0 = defaultCoverageRegion0.toString();
      assertEquals("coverage region : [ -1 .. -1 ]/0B coverage = 0", string0);
  }

  @Test
  public void test6()  throws Throwable  {
      PriorityQueue<Rangeable> priorityQueue0 = new PriorityQueue<Rangeable>();
      Integer integer0 = new Integer(622);
      DefaultCoverageRegion.Builder<Rangeable> defaultCoverageRegion_Builder0 = new DefaultCoverageRegion.Builder<Rangeable>((-1L), (Iterable<Rangeable>) priorityQueue0, integer0);
      assertNotNull(defaultCoverageRegion_Builder0);
      
      DefaultCoverageRegion.Builder<Rangeable> defaultCoverageRegion_Builder1 = (DefaultCoverageRegion.Builder<Rangeable>)defaultCoverageRegion_Builder0.end((-1L));
      assertEquals(true, defaultCoverageRegion_Builder0.isEndIsSet());
      
      Range.CoordinateSystem range_CoordinateSystem0 = Range.CoordinateSystem.SPACE_BASED;
      DirectedRange directedRange0 = DirectedRange.parse((long) 622, (long) 622, range_CoordinateSystem0);
      defaultCoverageRegion_Builder0.offer((Rangeable) directedRange0);
      DefaultCoverageRegion<Rangeable> defaultCoverageRegion0 = (DefaultCoverageRegion<Rangeable>)defaultCoverageRegion_Builder1.build();
      int int0 = defaultCoverageRegion0.hashCode();
      assertEquals((-1894990738), int0);
  }

  @Test
  public void test7()  throws Throwable  {
      PriorityQueue<Rangeable> priorityQueue0 = new PriorityQueue<Rangeable>();
      Integer integer0 = new Integer(621);
      DefaultCoverageRegion.Builder<Rangeable> defaultCoverageRegion_Builder0 = new DefaultCoverageRegion.Builder<Rangeable>((-1L), (Iterable<Rangeable>) priorityQueue0, integer0);
      assertNotNull(defaultCoverageRegion_Builder0);
      
      DefaultCoverageRegion.Builder<Rangeable> defaultCoverageRegion_Builder1 = (DefaultCoverageRegion.Builder<Rangeable>)defaultCoverageRegion_Builder0.end((-1L));
      Range.CoordinateSystem range_CoordinateSystem0 = Range.CoordinateSystem.RESIDUE_BASED;
      DirectedRange directedRange0 = DirectedRange.parse((long) integer0, (-1L), range_CoordinateSystem0);
      defaultCoverageRegion_Builder1.offer((Rangeable) directedRange0);
      DefaultCoverageRegion<Rangeable> defaultCoverageRegion0 = (DefaultCoverageRegion<Rangeable>)defaultCoverageRegion_Builder1.build();
      assertEquals(true, defaultCoverageRegion_Builder0.isEndIsSet());
      
      DefaultCoverageRegion<Rangeable> defaultCoverageRegion1 = (DefaultCoverageRegion<Rangeable>)defaultCoverageRegion_Builder1.build();
      boolean boolean0 = defaultCoverageRegion0.equals((Object) defaultCoverageRegion1);
      assertEquals(true, boolean0);
  }

  @Test
  public void test8()  throws Throwable  {
      PriorityQueue<Rangeable> priorityQueue0 = new PriorityQueue<Rangeable>();
      Integer integer0 = new Integer(622);
      DefaultCoverageRegion.Builder<Rangeable> defaultCoverageRegion_Builder0 = new DefaultCoverageRegion.Builder<Rangeable>((-1L), (Iterable<Rangeable>) priorityQueue0, integer0);
      assertNotNull(defaultCoverageRegion_Builder0);
      
      DefaultCoverageRegion.Builder<Rangeable> defaultCoverageRegion_Builder1 = (DefaultCoverageRegion.Builder<Rangeable>)defaultCoverageRegion_Builder0.end((-1L));
      DefaultCoverageRegion<Rangeable> defaultCoverageRegion0 = (DefaultCoverageRegion<Rangeable>)defaultCoverageRegion_Builder1.build();
      assertEquals(true, defaultCoverageRegion_Builder0.isEndIsSet());
      
      boolean boolean0 = defaultCoverageRegion0.equals((Object) defaultCoverageRegion_Builder1);
      assertEquals(false, boolean0);
  }

  @Test
  public void test9()  throws Throwable  {
      DefaultCoverageRegion.Builder<Rangeable> defaultCoverageRegion_Builder0 = null;
      try {
        defaultCoverageRegion_Builder0 = new DefaultCoverageRegion.Builder<Rangeable>((long) 7, (Iterable<Rangeable>) null);
        fail("Expecting exception: IllegalArgumentException");
      } catch(IllegalArgumentException e) {
        /*
         * elements can not be null
         */
      }
  }

  @Test
  public void test10()  throws Throwable  {
      PriorityQueue<Rangeable> priorityQueue0 = new PriorityQueue<Rangeable>();
      Integer integer0 = new Integer(622);
      DefaultCoverageRegion.Builder<Rangeable> defaultCoverageRegion_Builder0 = new DefaultCoverageRegion.Builder<Rangeable>((-7L), (Iterable<Rangeable>) priorityQueue0, integer0);
      assertNotNull(defaultCoverageRegion_Builder0);
      
      DefaultCoverageRegion.Builder<Rangeable> defaultCoverageRegion_Builder1 = (DefaultCoverageRegion.Builder<Rangeable>)defaultCoverageRegion_Builder0.end((-7L));
      Range.CoordinateSystem range_CoordinateSystem0 = Range.CoordinateSystem.RESIDUE_BASED;
      DirectedRange directedRange0 = DirectedRange.parse((long) 622, (long) integer0, range_CoordinateSystem0);
      defaultCoverageRegion_Builder1.offer((Rangeable) directedRange0);
      DefaultCoverageRegion<Rangeable> defaultCoverageRegion0 = (DefaultCoverageRegion<Rangeable>)defaultCoverageRegion_Builder1.build();
      assertEquals(true, defaultCoverageRegion_Builder0.isEndIsSet());
      
      Iterator<Rangeable> iterator0 = defaultCoverageRegion0.iterator();
      DefaultCoverageRegion.Builder<Rangeable> defaultCoverageRegion_Builder2 = new DefaultCoverageRegion.Builder<Rangeable>((-7L), iterator0);
      assertEquals(false, iterator0.hasNext());
  }

  @Test
  public void test11()  throws Throwable  {
      Stack<Rangeable> stack0 = new Stack<Rangeable>();
      ListIterator<Rangeable> listIterator0 = stack0.listIterator();
      DefaultCoverageRegion.Builder<Rangeable> defaultCoverageRegion_Builder0 = new DefaultCoverageRegion.Builder<Rangeable>(4294967304L, (Iterator<Rangeable>) listIterator0);
      assertNotNull(defaultCoverageRegion_Builder0);
      
      // Undeclared exception!
      try {
        defaultCoverageRegion_Builder0.end(853L);
        fail("Expecting exception: IllegalArgumentException");
      } catch(IllegalArgumentException e) {
        /*
         * end must be >= than 4294967305 but was 853
         */
      }
  }

  @Test
  public void test12()  throws Throwable  {
      Stack<Rangeable> stack0 = new Stack<Rangeable>();
      ListIterator<Rangeable> listIterator0 = stack0.listIterator();
      DefaultCoverageRegion.Builder<Rangeable> defaultCoverageRegion_Builder0 = new DefaultCoverageRegion.Builder<Rangeable>(4294967304L, (Iterator<Rangeable>) listIterator0);
      assertNotNull(defaultCoverageRegion_Builder0);
      
      DefaultCoverageRegion.Builder<Rangeable> defaultCoverageRegion_Builder1 = (DefaultCoverageRegion.Builder<Rangeable>)defaultCoverageRegion_Builder0.end(4294967304L);
      long long0 = defaultCoverageRegion_Builder1.end();
      assertEquals(true, defaultCoverageRegion_Builder0.isEndIsSet());
      assertEquals(4294967304L, long0);
  }

  @Test
  public void test13()  throws Throwable  {
      TreeSet<CoverageRegion<Rangeable>> treeSet0 = new TreeSet<CoverageRegion<Rangeable>>();
      Iterator<CoverageRegion<Rangeable>> iterator0 = treeSet0.iterator();
      DefaultCoverageRegion.Builder<CoverageRegion<Rangeable>> defaultCoverageRegion_Builder0 = new DefaultCoverageRegion.Builder<CoverageRegion<Rangeable>>((long) 622, iterator0, (Integer) 622);
      assertNotNull(defaultCoverageRegion_Builder0);
      
      // Undeclared exception!
      try {
        defaultCoverageRegion_Builder0.build();
        fail("Expecting exception: IllegalStateException");
      } catch(IllegalStateException e) {
        /*
         * end must be set
         */
      }
  }
}
