/*
 * This file was automatically generated by EvoSuite
 */

package org.jcvi.jillion.internal.trace.chromat.abi.tag;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import org.jcvi.jillion.internal.trace.chromat.abi.tag.DefaultTaggedDataRecord;
import org.jcvi.jillion.internal.trace.chromat.abi.tag.DefaultUserDefinedTaggedDataRecord;
import org.jcvi.jillion.internal.trace.chromat.abi.tag.TaggedDataName;
import org.jcvi.jillion.internal.trace.chromat.abi.tag.TaggedDataType;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class DefaultTaggedDataRecordEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      TaggedDataName taggedDataName0 = TaggedDataName.SCAN;
      TaggedDataType taggedDataType0 = TaggedDataType.CHAR;
      DefaultUserDefinedTaggedDataRecord defaultUserDefinedTaggedDataRecord0 = new DefaultUserDefinedTaggedDataRecord(taggedDataName0, (long) 0, taggedDataType0, 0, (long) 0, (-615L), (-615L), (long) 0);
      byte[] byteArray0 = new byte[8];
      defaultUserDefinedTaggedDataRecord0.parseDataRecordFrom(byteArray0);
  }

  @Test
  public void test1()  throws Throwable  {
      TaggedDataName taggedDataName0 = TaggedDataName.P2AM;
      TaggedDataType taggedDataType0 = TaggedDataType.DATE;
      DefaultTaggedDataRecord defaultTaggedDataRecord0 = new DefaultTaggedDataRecord(taggedDataName0, 0L, taggedDataType0, (-1250), 5L, 0L, 0L, 0L);
      defaultTaggedDataRecord0.getParsedDataType();
  }

  @Test
  public void test2()  throws Throwable  {
      TaggedDataName taggedDataName0 = TaggedDataName.TRANSFORM_MATRIX_FILE_NAME;
      TaggedDataType taggedDataType0 = TaggedDataType.FLOAT;
      DefaultUserDefinedTaggedDataRecord defaultUserDefinedTaggedDataRecord0 = new DefaultUserDefinedTaggedDataRecord(taggedDataName0, (-1L), taggedDataType0, 1603, (-1L), (-1L), (long) 1603, (-18L));
      defaultUserDefinedTaggedDataRecord0.getType();
  }
}
