/*
 * This file was automatically generated by EvoSuite
 */

package org.jcvi.jillion.assembly.ca.asm;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import org.jcvi.jillion.assembly.ca.asm.DefaultAsmAssembledRead;
import org.jcvi.jillion.core.Direction;
import org.jcvi.jillion.core.Range;
import org.jcvi.jillion.core.residue.nt.NucleotideSequence;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class DefaultAsmAssembledReadEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      Direction direction0 = Direction.REVERSE;
      // Undeclared exception!
      try {
        DefaultAsmAssembledRead.createBuilder((NucleotideSequence) null, "FN", "FN", (-5), direction0, (Range) null, (-5), false);
        fail("Expecting exception: IllegalArgumentException");
      } catch(IllegalArgumentException e) {
        /*
         * invalid character F ascii value 70
         */
      }
  }
}
