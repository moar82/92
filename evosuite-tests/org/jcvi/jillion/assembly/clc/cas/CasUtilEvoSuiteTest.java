/*
 * This file was automatically generated by EvoSuite
 */

package org.jcvi.jillion.assembly.clc.cas;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import java.io.ByteArrayInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.SequenceInputStream;
import org.jcvi.jillion.assembly.clc.cas.CasUtil;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class CasUtilEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      PipedInputStream pipedInputStream0 = new PipedInputStream();
      try {
        CasUtil.readCasUnsignedByte((InputStream) pipedInputStream0);
        fail("Expecting exception: IOException");
      } catch(IOException e) {
        /*
         * Pipe not connected
         */
      }
  }

  @Test
  public void test1()  throws Throwable  {
      byte[] byteArray0 = new byte[6];
      ByteArrayInputStream byteArrayInputStream0 = new ByteArrayInputStream(byteArray0, (int) (byte) (-2), (int) (byte)0);
      try {
        CasUtil.readCasUnsignedLong((InputStream) byteArrayInputStream0);
        fail("Expecting exception: EOFException");
      } catch(EOFException e) {
        /*
         * end of file after only 0 bytes read (expected 8)
         */
      }
  }

  @Test
  public void test2()  throws Throwable  {
      byte[] byteArray0 = new byte[6];
      byteArray0[0] = (byte) (-1);
      ByteArrayInputStream byteArrayInputStream0 = new ByteArrayInputStream(byteArray0);
      long long0 = CasUtil.parseByteCountFrom((InputStream) byteArrayInputStream0);
      assertEquals(3, byteArrayInputStream0.available());
      assertEquals(0L, long0);
  }

  @Test
  public void test3()  throws Throwable  {
      byte[] byteArray0 = new byte[3];
      ByteArrayInputStream byteArrayInputStream0 = new ByteArrayInputStream(byteArray0);
      SequenceInputStream sequenceInputStream0 = new SequenceInputStream((InputStream) byteArrayInputStream0, (InputStream) byteArrayInputStream0);
      CasUtil.parseCasStringFrom((InputStream) sequenceInputStream0);
  }

  @Test
  public void test4()  throws Throwable  {
      // Undeclared exception!
      try {
        CasUtil.readCasUnsignedShort((InputStream) null);
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
        /*
         * inputstream can not be null
         */
      }
  }

  @Test
  public void test5()  throws Throwable  {
      int int0 = CasUtil.numberOfBytesRequiredFor(761L);
      assertEquals(2, int0);
  }

  @Test
  public void test6()  throws Throwable  {
      // Undeclared exception!
      try {
        CasUtil.numberOfBytesRequiredFor(0L);
        fail("Expecting exception: IllegalArgumentException");
      } catch(IllegalArgumentException e) {
        /*
         * input number must be > 0 : 0
         */
      }
  }

  @Test
  public void test7()  throws Throwable  {
      try {
        CasUtil.getFileFor((File) null, "''dvYOE.dd7 l");
        fail("Expecting exception: FileNotFoundException");
      } catch(FileNotFoundException e) {
        /*
         * /mnt/fastdata/ac1gf/SF110/dist/92_jcvi-javacommon/''dvYOE.dd7 l
         */
      }
  }
}
