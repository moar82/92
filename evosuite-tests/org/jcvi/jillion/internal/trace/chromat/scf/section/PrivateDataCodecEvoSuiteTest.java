/*
 * This file was automatically generated by EvoSuite
 */

package org.jcvi.jillion.internal.trace.chromat.scf.section;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import java.io.DataInputStream;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.PipedInputStream;
import org.jcvi.jillion.internal.trace.chromat.scf.header.DefaultSCFHeader;
import org.jcvi.jillion.internal.trace.chromat.scf.header.SCFHeader;
import org.jcvi.jillion.internal.trace.chromat.scf.section.PrivateDataCodec;
import org.jcvi.jillion.internal.trace.chromat.scf.section.SectionDecoderException;
import org.jcvi.jillion.trace.chromat.ChromatogramFileVisitor;
import org.jcvi.jillion.trace.chromat.abi.AbiChromatogramBuilder;
import org.jcvi.jillion.trace.chromat.scf.ScfChromatogramBuilder;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class PrivateDataCodecEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      PrivateDataCodec privateDataCodec0 = new PrivateDataCodec();
      FileDescriptor fileDescriptor0 = FileDescriptor.in;
      FileInputStream fileInputStream0 = new FileInputStream(fileDescriptor0);
      DataInputStream dataInputStream0 = new DataInputStream((InputStream) fileInputStream0);
      DefaultSCFHeader defaultSCFHeader0 = new DefaultSCFHeader();
      // Undeclared exception!
      try {
        privateDataCodec0.decode(dataInputStream0, 0L, (SCFHeader) defaultSCFHeader0, (ScfChromatogramBuilder) null);
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test1()  throws Throwable  {
      PrivateDataCodec privateDataCodec0 = new PrivateDataCodec();
      PipedInputStream pipedInputStream0 = new PipedInputStream(103);
      DataInputStream dataInputStream0 = new DataInputStream((InputStream) pipedInputStream0);
      DefaultSCFHeader defaultSCFHeader0 = new DefaultSCFHeader();
      AbiChromatogramBuilder.AbiChromatogramBuilderVisitor abiChromatogramBuilder_AbiChromatogramBuilderVisitor0 = new AbiChromatogramBuilder.AbiChromatogramBuilderVisitor("Yi|M#E5b)l aI_xK_d4");
      long long0 = privateDataCodec0.decode(dataInputStream0, (long) 103, (SCFHeader) defaultSCFHeader0, (ChromatogramFileVisitor) abiChromatogramBuilder_AbiChromatogramBuilderVisitor0);
      assertEquals(0L, long0);
  }
}
