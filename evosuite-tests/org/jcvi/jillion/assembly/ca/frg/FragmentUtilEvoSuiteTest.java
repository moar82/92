/*
 * This file was automatically generated by EvoSuite
 */

package org.jcvi.jillion.assembly.ca.frg;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jcvi.jillion.assembly.ca.frg.FragmentUtil;
import org.jcvi.jillion.core.Range;
import org.jcvi.jillion.core.io.TextFileVisitor;
import org.jcvi.jillion.core.qual.QualitySequence;
import org.jcvi.jillion.core.residue.nt.NucleotideSequence;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class FragmentUtilEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      NucleotideSequence nucleotideSequence0 = FragmentUtil.parseBasesFrom("_.1[an/#a8X ");
      assertEquals(0L, nucleotideSequence0.getUngappedLength());
  }

  @Test
  public void test1()  throws Throwable  {
      Range range0 = FragmentUtil.parseValidRangeFrom("aO5V[p71802tdi~7");
      assertNull(range0);
  }

  @Test
  public void test2()  throws Throwable  {
      PipedInputStream pipedInputStream0 = new PipedInputStream();
      Scanner scanner0 = new Scanner((InputStream) pipedInputStream0);
      String string0 = FragmentUtil.readRestOfBlock(scanner0, (TextFileVisitor) null);
      assertEquals("", string0);
  }

  @Test
  public void test3()  throws Throwable  {
      Scanner scanner0 = new Scanner("2[/;L5iAFeU$3C&]");
      // Undeclared exception!
      String string0 = "";
      try {
        string0 = FragmentUtil.readRestOfBlock(scanner0, (TextFileVisitor) null);
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test4()  throws Throwable  {
      QualitySequence qualitySequence0 = FragmentUtil.parseEncodedQualitySequence("aO5V[p71802tdi~7");
      assertEquals(16L, qualitySequence0.getLength());
      assertNotNull(qualitySequence0);
  }

  @Test
  public void test5()  throws Throwable  {
      // Undeclared exception!
      try {
        FragmentUtil.parseEncodedQualitySequence("_.1[an/#a8X ");
        fail("Expecting exception: IllegalArgumentException");
      } catch(IllegalArgumentException e) {
        /*
         * initial capacity should be > 0 :0
         */
      }
  }

  @Test
  public void test6()  throws Throwable  {
      Pattern pattern0 = Pattern.compile("", 3);
      Matcher matcher0 = pattern0.matcher((CharSequence) "");
      // Undeclared exception!
      Range range0 = null;
      try {
        range0 = FragmentUtil.parseRangeFrom(matcher0);
        fail("Expecting exception: IndexOutOfBoundsException");
      } catch(IndexOutOfBoundsException e) {
        /*
         * No group 1
         */
      }
  }

  @Test
  public void test7()  throws Throwable  {
      // Undeclared exception!
      try {
        FragmentUtil.parseBasesFrom("aO5V[p71802tdi~7");
        fail("Expecting exception: IllegalArgumentException");
      } catch(IllegalArgumentException e) {
        /*
         * invalid character O ascii value 79
         */
      }
  }
}
