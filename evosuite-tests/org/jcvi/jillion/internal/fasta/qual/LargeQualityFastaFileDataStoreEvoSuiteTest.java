/*
 * This file was automatically generated by EvoSuite
 */

package org.jcvi.jillion.internal.fasta.qual;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import java.io.File;
import org.jcvi.jillion.internal.fasta.qual.LargeQualityFastaFileDataStore;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class LargeQualityFastaFileDataStoreEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      File file0 = new File("wU['-y:$BH|sbj", "wU['-y:$BH|sbj");
      LargeQualityFastaFileDataStore largeQualityFastaFileDataStore0 = (LargeQualityFastaFileDataStore)LargeQualityFastaFileDataStore.create(file0);
      assertEquals(false, largeQualityFastaFileDataStore0.isClosed());
  }
}
