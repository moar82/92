/*
 * This file was automatically generated by EvoSuite
 */

package org.jcvi.jillion.internal.trace.chromat.scf.section;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import org.jcvi.jillion.core.pos.PositionSequence;
import org.jcvi.jillion.internal.trace.chromat.scf.header.pos.BytePositionStrategy;
import org.jcvi.jillion.internal.trace.chromat.scf.header.pos.PositionStrategy;
import org.jcvi.jillion.internal.trace.chromat.scf.section.Version2SampleSectionCodec;
import org.jcvi.jillion.trace.chromat.scf.ScfChromatogramBuilder;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class Version2SampleSectionCodecEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      Version2SampleSectionCodec version2SampleSectionCodec0 = new Version2SampleSectionCodec();
      BytePositionStrategy bytePositionStrategy0 = new BytePositionStrategy();
      short[][] shortArray0 = new short[6][8];
      version2SampleSectionCodec0.extractActualPositions((PositionStrategy) bytePositionStrategy0, shortArray0);
      assertEquals(1, bytePositionStrategy0.getSampleSize());
  }

  @Test
  public void test1()  throws Throwable  {
      BytePositionStrategy bytePositionStrategy0 = new BytePositionStrategy();
      ScfChromatogramBuilder scfChromatogramBuilder0 = new ScfChromatogramBuilder("24OF7^&i~");
      short[] shortArray0 = new short[1];
      scfChromatogramBuilder0.peaks(shortArray0);
      PositionSequence positionSequence0 = scfChromatogramBuilder0.peaks();
      Version2SampleSectionCodec version2SampleSectionCodec0 = new Version2SampleSectionCodec();
      // Undeclared exception!
      try {
        version2SampleSectionCodec0.writePositionsToBuffer((PositionStrategy) bytePositionStrategy0, positionSequence0, positionSequence0, positionSequence0, positionSequence0, (ByteBuffer) null);
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test2()  throws Throwable  {
      Version2SampleSectionCodec version2SampleSectionCodec0 = new Version2SampleSectionCodec();
      byte[] byteArray0 = new byte[4];
      ByteArrayInputStream byteArrayInputStream0 = new ByteArrayInputStream(byteArray0);
      DataInputStream dataInputStream0 = new DataInputStream((InputStream) byteArrayInputStream0);
      BytePositionStrategy bytePositionStrategy0 = new BytePositionStrategy();
      version2SampleSectionCodec0.parseRawPositions(dataInputStream0, (int) (byte)1, (PositionStrategy) bytePositionStrategy0);
  }
}
