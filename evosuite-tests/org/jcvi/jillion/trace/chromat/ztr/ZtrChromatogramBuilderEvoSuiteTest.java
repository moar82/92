/*
 * This file was automatically generated by EvoSuite
 */

package org.jcvi.jillion.trace.chromat.ztr;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import org.jcvi.jillion.core.Range;
import org.jcvi.jillion.core.pos.PositionSequence;
import org.jcvi.jillion.core.residue.nt.NucleotideSequence;
import org.jcvi.jillion.trace.TraceDecoderException;
import org.jcvi.jillion.trace.chromat.ztr.ZtrChromatogram;
import org.jcvi.jillion.trace.chromat.ztr.ZtrChromatogramBuilder;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class ZtrChromatogramBuilderEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      ZtrChromatogramBuilder ztrChromatogramBuilder0 = new ZtrChromatogramBuilder(":Pk|k{UwM C");
      ZtrChromatogramBuilder ztrChromatogramBuilder1 = ztrChromatogramBuilder0.basecalls((NucleotideSequence) null);
      assertSame(ztrChromatogramBuilder0, ztrChromatogramBuilder1);
  }

  @Test
  public void test1()  throws Throwable  {
      ZtrChromatogramBuilder ztrChromatogramBuilder0 = new ZtrChromatogramBuilder("Hnag)N7Q=l1p?");
      PositionSequence positionSequence0 = ztrChromatogramBuilder0.peaks();
      assertNull(positionSequence0);
  }

  @Test
  public void test2()  throws Throwable  {
      File file0 = new File("AC@!';1V", "AC@!';1V");
      ZtrChromatogramBuilder ztrChromatogramBuilder0 = null;
      try {
        ztrChromatogramBuilder0 = new ZtrChromatogramBuilder("AC@!';1V", file0);
        fail("Expecting exception: FileNotFoundException");
      } catch(FileNotFoundException e) {
        /*
         * AC@!';1V/AC@!';1V (No such file or directory)
         */
      }
  }

  @Test
  public void test3()  throws Throwable  {
      ZtrChromatogramBuilder ztrChromatogramBuilder0 = new ZtrChromatogramBuilder(":Y'3\"3h");
      byte[] byteArray0 = new byte[24];
      ZtrChromatogramBuilder ztrChromatogramBuilder1 = ztrChromatogramBuilder0.tConfidence(byteArray0);
      assertSame(ztrChromatogramBuilder1, ztrChromatogramBuilder0);
  }

  @Test
  public void test4()  throws Throwable  {
      ZtrChromatogramBuilder ztrChromatogramBuilder0 = new ZtrChromatogramBuilder("-");
      byte[] byteArray0 = ztrChromatogramBuilder0.gConfidence();
      assertNotNull(byteArray0);
  }

  @Test
  public void test5()  throws Throwable  {
      ZtrChromatogramBuilder ztrChromatogramBuilder0 = new ZtrChromatogramBuilder(";%MN\"WyZ2O0h|T");
      byte[] byteArray0 = ztrChromatogramBuilder0.cConfidence();
      assertNotNull(byteArray0);
  }

  @Test
  public void test6()  throws Throwable  {
      ZtrChromatogramBuilder ztrChromatogramBuilder0 = new ZtrChromatogramBuilder(":Y'3\"3h");
      // Undeclared exception!
      try {
        ztrChromatogramBuilder0.build();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test7()  throws Throwable  {
      ZtrChromatogramBuilder ztrChromatogramBuilder0 = new ZtrChromatogramBuilder(";%MN\"WyZ2O0h|T");
      byte[] byteArray0 = ztrChromatogramBuilder0.tConfidence();
      assertNotNull(byteArray0);
  }

  @Test
  public void test8()  throws Throwable  {
      byte[] byteArray0 = new byte[6];
      ByteArrayInputStream byteArrayInputStream0 = new ByteArrayInputStream(byteArray0, (int) (byte) (-1), (int) (byte) (-1));
      ZtrChromatogramBuilder ztrChromatogramBuilder0 = null;
      try {
        ztrChromatogramBuilder0 = new ZtrChromatogramBuilder("T$", (InputStream) byteArrayInputStream0);
        fail("Expecting exception: TraceDecoderException");
      } catch(TraceDecoderException e) {
        /*
         * error parsing ztr header
         */
      }
  }

  @Test
  public void test9()  throws Throwable  {
      ZtrChromatogramBuilder ztrChromatogramBuilder0 = new ZtrChromatogramBuilder(":Pk|k{UwM C");
      NucleotideSequence nucleotideSequence0 = ztrChromatogramBuilder0.basecalls();
      assertNull(nucleotideSequence0);
  }

  @Test
  public void test10()  throws Throwable  {
      ZtrChromatogramBuilder ztrChromatogramBuilder0 = new ZtrChromatogramBuilder("-");
      byte[] byteArray0 = new byte[5];
      ZtrChromatogramBuilder ztrChromatogramBuilder1 = ztrChromatogramBuilder0.cConfidence(byteArray0);
      assertSame(ztrChromatogramBuilder0, ztrChromatogramBuilder1);
  }

  @Test
  public void test11()  throws Throwable  {
      ZtrChromatogramBuilder ztrChromatogramBuilder0 = new ZtrChromatogramBuilder("-");
      ZtrChromatogramBuilder ztrChromatogramBuilder1 = ztrChromatogramBuilder0.clip((Range) null);
      assertSame(ztrChromatogramBuilder1, ztrChromatogramBuilder0);
  }

  @Test
  public void test12()  throws Throwable  {
      ZtrChromatogramBuilder ztrChromatogramBuilder0 = null;
      try {
        ztrChromatogramBuilder0 = new ZtrChromatogramBuilder((ZtrChromatogram) null);
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test13()  throws Throwable  {
      ZtrChromatogramBuilder ztrChromatogramBuilder0 = new ZtrChromatogramBuilder("@MJXq&Di");
      short[] shortArray0 = ztrChromatogramBuilder0.cPositions();
      assertNotNull(shortArray0);
  }

  @Test
  public void test14()  throws Throwable  {
      ZtrChromatogramBuilder ztrChromatogramBuilder0 = new ZtrChromatogramBuilder("-");
      short[] shortArray0 = new short[8];
      ZtrChromatogramBuilder ztrChromatogramBuilder1 = ztrChromatogramBuilder0.cPositions(shortArray0);
      assertSame(ztrChromatogramBuilder1, ztrChromatogramBuilder0);
  }

  @Test
  public void test15()  throws Throwable  {
      ZtrChromatogramBuilder ztrChromatogramBuilder0 = new ZtrChromatogramBuilder(";%MN\"WyZ2O0h|T");
      short[] shortArray0 = new short[6];
      ZtrChromatogramBuilder ztrChromatogramBuilder1 = ztrChromatogramBuilder0.gPositions(shortArray0);
      assertSame(ztrChromatogramBuilder0, ztrChromatogramBuilder1);
  }

  @Test
  public void test16()  throws Throwable  {
      ZtrChromatogramBuilder ztrChromatogramBuilder0 = new ZtrChromatogramBuilder(";%MN\"WyZ2O0h|T");
      short[] shortArray0 = ztrChromatogramBuilder0.tPositions();
      assertNotNull(shortArray0);
  }

  @Test
  public void test17()  throws Throwable  {
      ZtrChromatogramBuilder ztrChromatogramBuilder0 = new ZtrChromatogramBuilder(";%MN\"WyZ2O0h|T");
      short[] shortArray0 = ztrChromatogramBuilder0.aPositions();
      assertNotNull(shortArray0);
  }

  @Test
  public void test18()  throws Throwable  {
      ZtrChromatogramBuilder ztrChromatogramBuilder0 = new ZtrChromatogramBuilder(":Y'3\"3h");
      HashMap<String, String> hashMap0 = new HashMap<String, String>();
      ZtrChromatogramBuilder ztrChromatogramBuilder1 = ztrChromatogramBuilder0.properties((Map<String, String>) hashMap0);
      assertSame(ztrChromatogramBuilder0, ztrChromatogramBuilder1);
  }

  @Test
  public void test19()  throws Throwable  {
      ZtrChromatogramBuilder ztrChromatogramBuilder0 = new ZtrChromatogramBuilder(":Pk|k{UwM C");
      Map<String, String> map0 = ztrChromatogramBuilder0.properties();
      assertNull(map0);
  }

  @Test
  public void test20()  throws Throwable  {
      ZtrChromatogramBuilder ztrChromatogramBuilder0 = new ZtrChromatogramBuilder("-");
      Range range0 = ztrChromatogramBuilder0.clip();
      assertNull(range0);
  }

  @Test
  public void test21()  throws Throwable  {
      ZtrChromatogramBuilder ztrChromatogramBuilder0 = new ZtrChromatogramBuilder(";%MN\"WyZ2O0h|T");
      short[] shortArray0 = new short[6];
      ZtrChromatogramBuilder ztrChromatogramBuilder1 = ztrChromatogramBuilder0.aPositions(shortArray0);
      assertSame(ztrChromatogramBuilder1, ztrChromatogramBuilder0);
  }

  @Test
  public void test22()  throws Throwable  {
      ZtrChromatogramBuilder ztrChromatogramBuilder0 = new ZtrChromatogramBuilder("-");
      short[] shortArray0 = new short[8];
      ZtrChromatogramBuilder ztrChromatogramBuilder1 = ztrChromatogramBuilder0.tPositions(shortArray0);
      assertSame(ztrChromatogramBuilder0, ztrChromatogramBuilder1);
  }

  @Test
  public void test23()  throws Throwable  {
      ZtrChromatogramBuilder ztrChromatogramBuilder0 = new ZtrChromatogramBuilder("-");
      byte[] byteArray0 = new byte[5];
      ZtrChromatogramBuilder ztrChromatogramBuilder1 = ztrChromatogramBuilder0.aConfidence(byteArray0);
      assertSame(ztrChromatogramBuilder1, ztrChromatogramBuilder0);
  }

  @Test
  public void test24()  throws Throwable  {
      ZtrChromatogramBuilder ztrChromatogramBuilder0 = new ZtrChromatogramBuilder(";%MN\"WyZ2O0h|T");
      short[] shortArray0 = new short[6];
      ZtrChromatogramBuilder ztrChromatogramBuilder1 = ztrChromatogramBuilder0.peaks(shortArray0);
      assertSame(ztrChromatogramBuilder1, ztrChromatogramBuilder0);
  }

  @Test
  public void test25()  throws Throwable  {
      ZtrChromatogramBuilder ztrChromatogramBuilder0 = new ZtrChromatogramBuilder(":Pk|k{UwM C");
      byte[] byteArray0 = new byte[10];
      ZtrChromatogramBuilder ztrChromatogramBuilder1 = ztrChromatogramBuilder0.gConfidence(byteArray0);
      assertSame(ztrChromatogramBuilder1, ztrChromatogramBuilder0);
  }

  @Test
  public void test26()  throws Throwable  {
      ZtrChromatogramBuilder ztrChromatogramBuilder0 = new ZtrChromatogramBuilder(":Y'3\"3h");
      short[] shortArray0 = ztrChromatogramBuilder0.gPositions();
      assertNotNull(shortArray0);
  }

  @Test
  public void test27()  throws Throwable  {
      ZtrChromatogramBuilder ztrChromatogramBuilder0 = new ZtrChromatogramBuilder(";%MN\"WyZ2O0h|T");
      byte[] byteArray0 = ztrChromatogramBuilder0.aConfidence();
      assertNotNull(byteArray0);
  }
}
