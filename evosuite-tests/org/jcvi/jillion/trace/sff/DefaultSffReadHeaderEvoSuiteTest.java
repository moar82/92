/*
 * This file was automatically generated by EvoSuite
 */

package org.jcvi.jillion.trace.sff;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import org.jcvi.jillion.core.Range;
import org.jcvi.jillion.trace.sff.DefaultSffReadHeader;
import org.jcvi.jillion.trace.sff.SffReadHeader;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class DefaultSffReadHeaderEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      DefaultSffReadHeader defaultSffReadHeader0 = new DefaultSffReadHeader(702, (Range) null, (Range) null, (String) null);
      DefaultSffReadHeader.Builder defaultSffReadHeader_Builder0 = new DefaultSffReadHeader.Builder((SffReadHeader) defaultSffReadHeader0);
      defaultSffReadHeader_Builder0.qualityClip((Range) null);
      assertEquals(702, defaultSffReadHeader0.getNumberOfBases());
  }

  @Test
  public void test1()  throws Throwable  {
      DefaultSffReadHeader defaultSffReadHeader0 = new DefaultSffReadHeader(702, (Range) null, (Range) null, (String) null);
      DefaultSffReadHeader.Builder defaultSffReadHeader_Builder0 = new DefaultSffReadHeader.Builder((SffReadHeader) defaultSffReadHeader0);
      DefaultSffReadHeader defaultSffReadHeader1 = (DefaultSffReadHeader)defaultSffReadHeader_Builder0.build();
      assertEquals(702, defaultSffReadHeader0.getNumberOfBases());
      assertTrue(defaultSffReadHeader1.equals(defaultSffReadHeader0));
  }

  @Test
  public void test2()  throws Throwable  {
      DefaultSffReadHeader defaultSffReadHeader0 = new DefaultSffReadHeader(220, (Range) null, (Range) null, "Z0");
      int int0 = defaultSffReadHeader0.hashCode();
      assertEquals(85681799, int0);
  }

  @Test
  public void test3()  throws Throwable  {
      DefaultSffReadHeader defaultSffReadHeader0 = new DefaultSffReadHeader(702, (Range) null, (Range) null, (String) null);
      int int0 = defaultSffReadHeader0.hashCode();
      assertEquals(1598143, int0);
  }

  @Test
  public void test4()  throws Throwable  {
      DefaultSffReadHeader defaultSffReadHeader0 = new DefaultSffReadHeader(220, (Range) null, (Range) null, "Z0");
      DefaultSffReadHeader defaultSffReadHeader1 = new DefaultSffReadHeader(220, (Range) null, (Range) null, "");
      boolean boolean0 = defaultSffReadHeader1.equals((Object) defaultSffReadHeader0);
      assertEquals(false, boolean0);
      assertEquals(220, defaultSffReadHeader0.getNumberOfBases());
  }

  @Test
  public void test5()  throws Throwable  {
      DefaultSffReadHeader defaultSffReadHeader0 = new DefaultSffReadHeader(702, (Range) null, (Range) null, (String) null);
      boolean boolean0 = defaultSffReadHeader0.equals((Object) null);
      assertEquals(false, boolean0);
      assertEquals(702, defaultSffReadHeader0.getNumberOfBases());
  }

  @Test
  public void test6()  throws Throwable  {
      DefaultSffReadHeader defaultSffReadHeader0 = new DefaultSffReadHeader(702, (Range) null, (Range) null, (String) null);
      DefaultSffReadHeader defaultSffReadHeader1 = new DefaultSffReadHeader(255, (Range) null, (Range) null, (String) null);
      boolean boolean0 = defaultSffReadHeader0.equals((Object) defaultSffReadHeader1);
      assertEquals(false, boolean0);
      assertEquals(255, defaultSffReadHeader1.getNumberOfBases());
  }

  @Test
  public void test7()  throws Throwable  {
      DefaultSffReadHeader defaultSffReadHeader0 = new DefaultSffReadHeader(702, (Range) null, (Range) null, (String) null);
      DefaultSffReadHeader defaultSffReadHeader1 = new DefaultSffReadHeader(702, (Range) null, (Range) null, (String) null);
      boolean boolean0 = defaultSffReadHeader0.equals((Object) defaultSffReadHeader1);
      assertEquals(702, defaultSffReadHeader0.getNumberOfBases());
      assertEquals(true, boolean0);
  }
}
