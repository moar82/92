/*
 * This file was automatically generated by EvoSuite
 */

package org.jcvi.jillion.assembly.clc.cas.consed;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import org.jcvi.jillion.assembly.clc.cas.consed.PhdReadRecord;
import org.jcvi.jillion.assembly.consed.ace.PhdInfo;
import org.jcvi.jillion.assembly.consed.phd.Phd;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class PhdReadRecordEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      PhdReadRecord phdReadRecord0 = null;
      try {
        phdReadRecord0 = new PhdReadRecord((Phd) null, (PhdInfo) null);
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
        /*
         * phd can not be null
         */
      }
  }
}
